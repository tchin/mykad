package peersim.emptyKad;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;



import peersim.transport.Transport;




public class EmptyKadProtocol implements EDProtocol{
	// Configuration parameter
	public String prefix;
	protected Parameters p;
	// VARIABLE PARAMETERS
	final String PAR_K = "K";
	final String PAR_ALPHA = "ALPHA";
	final String PAR_BITS = "BITS";	
	/**
	 * allow to call the service initializer only once
	 */
	private static boolean _ALREADY_INSTALLED = false;
	
	
	//current refering node
	protected Node node;
	
	//Kad information
	protected BigInteger kadId;	//identifier
//	public boolean joinState; //join state
	protected final int idLength; //bit lenght use	
	/**
	 * routing table of this pastry node
	 */
	private RoutingTable routingTable;
	/**
	 * trace message sent for timeout purpose
	 */
	private TreeMap<Long, Long> sentMsg;	
	
	
	/**
	 * find operations set
	 */
	private LinkedHashMap<Long, FindOperation> findOp;	
	public RoutingTable getRoutingTable(){
		return routingTable;
	}
	public EmptyKadProtocol(String prefix) {
		super();
		// set prefix
		this.prefix = prefix;
		

		_init();
		idLength = Configuration.getInt(prefix + "."
				+ PeersimConfig.PAR_IDLENGTH);		
		this.kadId = KadIdGenerator.generateID(idLength);
		routingTable = new RoutingTable(kadId);

		sentMsg = new TreeMap<Long, Long>();

		findOp = new LinkedHashMap<Long, FindOperation>();
		
		p = new Parameters();
		p.tid = Configuration
				.getPid(prefix + "." + PeersimConfig.PAR_TRANSPORT);		

		
		

	}	
	
	/**
	 * This procedure is called only once and allow to inizialize the internal state of KademliaProtocol. Every node shares the
	 * same configuration, so it is sufficient to call this routine once.
	 */
	private void _init() {
		// execute once
		if (_ALREADY_INSTALLED)
			return;

		// read paramaters
		KademliaCommonConfig.K = Configuration.getInt(prefix + "." + PAR_K, KademliaCommonConfig.K);
		KademliaCommonConfig.ALPHA = Configuration.getInt(prefix + "." + PAR_ALPHA, KademliaCommonConfig.ALPHA);
		KademliaCommonConfig.BITS = Configuration.getInt(prefix + "." + PAR_BITS, KademliaCommonConfig.BITS);

		_ALREADY_INSTALLED = true;
	}	
	public void setNode(Node n) {
		this.node = n;
		this.routingTable.nodeId = ((EmptyKadProtocol)n.getProtocol(p.pid)).getKadId();
	}	
	
	public BigInteger getKadId() {
		return kadId;
	}	
	
	public void create(){
		getRoutingTable().addNeighbour(getKadId());
	}
	
	public int getTranspotProtocol() {
		return p.tid;
	}	
	@Override
	public void processEvent(Node node, int myPid, Object event) {
		// TODO Auto-generated method stub

		// Parse message content Activate the correct event manager fot the particular event
		this.p.pid = myPid;

		if (this.node.isUp()) {
		Message m;
		
//		System.out.println("$$" + ((SimpleEvent) event).getType());
		switch (((SimpleEvent) event).getType()) {

			case Message.MSG_RESPONSE:
				m = (Message) event;
				sentMsg.remove(m.ackId);
				route(m, myPid);
				break;

			case Message.MSG_FINDNODE:
				m = (Message) event;
				find(m, myPid);
				break;

			case Message.MSG_ROUTE:

				m = (Message) event;
				routeResponse(m, myPid);
				break;

			case Message.MSG_EMPTY:
				// TO DO
				break;

			case Message.MSG_STORE:
				// TO DO
				break;

			case Timeout.TIMEOUT: // timeout
				Timeout t = (Timeout) event;
				if (sentMsg.containsKey(t.msgID)) { // the response msg isn't arrived
					// remove form sentMsg
					sentMsg.remove(t.msgID);
					// remove node from my routing table
					this.routingTable.removeNeighbour(t.node);
					// remove from closestSet of find operation
					this.findOp.get(t.opID).closestSet.remove(t.node);
					// try another node
					Message m1 = new Message();
					m1.operationId = t.opID;
					m1.src = kadId;
					m1.dest = this.findOp.get(t.opID).destNode;
					this.route(m1, myPid);
				}
				break;

		}
		
		
		}
		
		
	}
	
	
	/**
	 * Response to a route request.<br>
	 * Find the ALPHA closest node consulting the k-buckets and return them to the sender.
	 * 
	 * @param m
	 *            Message
	 * @param myPid
	 *            the sender Pid
	 */
	private void routeResponse(Message m, int myPid) {
		// get the ALPHA closest node to destNode
		BigInteger[] neighbours = this.routingTable.getNeighbours(m.dest, m.src);

		// create a response message containing the neighbours (with the same id of the request)
		Message response = new Message(Message.MSG_RESPONSE, neighbours);
		response.operationId = m.operationId;
		response.dest = m.dest;
		response.src = this.getKadId();
		response.ackId = m.id; // set ACK number

		// send back the neighbours to the source of the message
		sendMessage(response, m.src, myPid);
	}
	
	
	/**
	 * Perform the required operation upon receiving a message in response to a ROUTE message.<br>
	 * Update the find operation record with the closest set of neighbour received. Than, send as many ROUTE request I can
	 * (according to the ALPHA parameter).<br>
	 * If no closest neighbour available and no outstanding messages stop the find operation.
	 * 
	 * @param m
	 *            Message
	 * @param myPid
	 *            the sender Pid
	 */
	private void route(Message m, int myPid) {
		// add message source to my routing table
		if (m.src != null) {
			routingTable.addNeighbour(m.src);
		}

		// get corresponding find operation (using the message field operationId)
		FindOperation fop = this.findOp.get(m.operationId);

		if (fop != null) {
			// save received neighbour in the closest Set of fin operation
			try {
				fop.elaborateResponse((BigInteger[]) m.body);
			} catch (Exception ex) {
				fop.available_requests++;
			}

			while (fop.available_requests > 0) { // I can send a new find request

				// get an available neighbour
				BigInteger neighbour = fop.getNeighbour();

				if (neighbour != null) {
					// create a new request to send to neighbour
					Message request = new Message(Message.MSG_ROUTE);
					request.operationId = m.operationId;
					request.src = this.kadId;
					request.dest = m.dest;

					// increment hop count
					fop.nrHops++;

					// send find request
					sendMessage(request, neighbour, myPid);
				} else if (fop.available_requests == KademliaCommonConfig.ALPHA) { // no new neighbour and no outstanding requests
					// search operation finished
					findOp.remove(fop.operationId);

					if (fop.body.equals("Automatically Generated Traffic") && fop.closestSet.containsKey(fop.destNode)) {
						// update statistics
						long timeInterval = (CommonState.getTime()) - (fop.timestamp);
						KademliaObserver.timeStore.add(timeInterval);
						KademliaObserver.hopStore.add(fop.nrHops);
						KademliaObserver.msg_deliv.add(1);
					}

					return;

				} else { // no neighbour available but exists oustanding request to wait
					return;
				}
			}
		} else {
			System.err.println("There has been some error in the protocol");
		}
	}
	/**
	 * send a message with current transport layer and starting the timeout timer (wich is an event) if the message is a request
	 * 
	 * @param m
	 *            the message to send
	 * @param destId
	 *            the Id of the destination node
	 * @param myPid
	 *            the sender Pid
	 */
	public void sendMessage(Message m, BigInteger destId, int myPid) {
	
		
		// add destination to routing table
		this.routingTable.addNeighbour(destId);

		Node src = nodeIdtoNode(this.kadId);
		Node dest = nodeIdtoNode(destId);

		//transport = (UnreliableTransport) (Network.prototype).getProtocol(tid);
		//transport.send(src, dest, m, kademliaid);

		if (m.getType() == Message.MSG_ROUTE) { // is a request
			//Timeout t = new Timeout(destId, m.id, m.operationId);
			//long latency = transport.getLatency(src, dest);

			// add to sent msg
			this.sentMsg.put(m.id, m.timestamp);
			Transport t = (Transport) this.node
					.getProtocol(((EmptyKadProtocol) node.getProtocol(p.pid))
							.getTranspotProtocol());
			
			t.send(src, dest, m, p.pid);
			//EDSimulator.add(4 * latency, t, src, myPid); // set delay = 2*RTT
		} else if(m.getType() == Message.MSG_RESPONSE) {
			Transport t = (Transport) this.node
					.getProtocol(((EmptyKadProtocol) node.getProtocol(p.pid))
							.getTranspotProtocol());
			
			t.send(src, dest, m, p.pid);			
		}
	}	
	
	
	/**
	 * Start a find node opearation.<br>
	 * Find the ALPHA closest node and send find request to them.
	 * 
	 * @param m
	 *            Message received (contains the node to find)
	 * @param myPid
	 *            the sender Pid
	 */
	private void find(Message m, int myPid) {

		KademliaObserver.find_op.add(1);

		// create find operation and add to operations array
		FindOperation fop = new FindOperation(m.dest, m.timestamp);
		fop.body = m.body;
		findOp.put(fop.operationId, fop);

		// get the ALPHA closest node to srcNode and add to find operation

		BigInteger[] neighbours = this.routingTable.getNeighbours(m.src, this.getKadId());
		
		fop.elaborateResponse(neighbours);


		
		fop.available_requests = KademliaCommonConfig.ALPHA;
		
		// set message operation id
		m.operationId = fop.operationId;
		m.type = Message.MSG_ROUTE;
		m.src = this.getKadId();

		// send ALPHA messages
		for (int i = 0; i < KademliaCommonConfig.ALPHA; i++) {
			BigInteger nextNode = fop.getNeighbour();
			
			if (nextNode != null) {
				sendMessage(m.copy(), nextNode, myPid);
				fop.nrHops++;
			}
		}

	}	
	/**
	 * Search through the network the Node having a specific node Id, by performing binary serach (we concern about the ordering
	 * of the network).
	 * 
	 * if you have full knowledge of the network you could look through every single piece of node and find the node id.
	 * else how can you do this? The function should not be the part of the kademia protocol.
	 * need this due to the routing tabe did not store node reference instead
	 * 
	 * @param searchNodeId
	 *            BigInteger
	 * @return Node
	 */
	public  Node nodeIdtoNode(BigInteger searchNodeId) {
		if (searchNodeId == null)
			return null;

		int inf = 0;
		int sup = Network.size() - 1;
		int m;

		while (inf <= sup) {
			m = (inf + sup) / 2;

			BigInteger mId = ((EmptyKadProtocol) Network.get(m).getProtocol(p.pid)).getKadId();

			if (mId.equals(searchNodeId))
				return Network.get(m);

			if (mId.compareTo(searchNodeId) < 0)
				inf = m + 1;
			else
				sup = m - 1;
		}

		// perform a traditional search for more reliability (maybe the network is not ordered)
		BigInteger mId;
		for (int i = Network.size() - 1; i >= 0; i--) {
			mId = ((EmptyKadProtocol) Network.get(i).getProtocol(p.pid)).getKadId();
			if (mId.equals(searchNodeId))
				return Network.get(i);
		}

		return null;
	}	
	// clone is call by the Dynamic Network to be added to network
	public Object clone() {
		EmptyKadProtocol cp = new EmptyKadProtocol(prefix);
		return cp;
	}
}
