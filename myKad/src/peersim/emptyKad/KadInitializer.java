package peersim.emptyKad;

import java.util.Random;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;
import peersim.edsim.EDSimulator;


import peersim.transport.Transport;

public class KadInitializer implements NodeInitializer {

	private int pid = 0;
	private int fail = 0; // counter for fail to find bootstrap server
	private EmptyKadProtocol currentp;
	public static boolean created = false;
	public KadInitializer(String prefix) {
		pid = Configuration.getPid(prefix + "." + PeersimConfig.PAR_PROT);
	}

	// when initialize is call , the node n is already
	// existed
	public void initialize(Node current) {
		currentp = (EmptyKadProtocol) current.getProtocol(pid);
		currentp.setNode(current);
		join(current);
	}

	public void join(final Node current) {

		// TODO Auto-generated method stub
		Node bootStrapNode = getBootStrapingNode();
		// Sending message looking for successor through BootStrapping
		// Node
		if (bootStrapNode != null) {
			//insert bootstrap into one of its k-buckets
			((EmptyKadProtocol) current.getProtocol(pid))
			.getRoutingTable().addNeighbour(((EmptyKadProtocol) bootStrapNode.getProtocol(pid)).getKadId());
			((EmptyKadProtocol) current.getProtocol(pid))
			.getRoutingTable().addNeighbour(((EmptyKadProtocol) current.getProtocol(pid)).getKadId());
			
			//do find_node of its own ID against the bootstrap

			// create auto-search message (searc message with destination my own Id)
			Message m = Message.makeFindNode("Bootstrap traffic");
			m.timestamp = CommonState.getTime();
			m.src = ((EmptyKadProtocol) current.getProtocol(pid)).getKadId();
			m.dest = ((EmptyKadProtocol) bootStrapNode.getProtocol(pid)).getKadId();
			//System.out.println("--------------" + ((EmptyKadProtocol) current.getProtocol(pid)).getKadId() + "%%%%%%%%%%%%%%" +((EmptyKadProtocol) bootStrapNode.getProtocol(pid)).getKadId());
			//EDSimulator.add(0, m, current, pid);		
			
			
			Transport t = (Transport) current
					.getProtocol(((EmptyKadProtocol) current.getProtocol(pid))
							.getTranspotProtocol());

			t.send(current, bootStrapNode, m, pid);

		} else {
			System.out.println("Should be only once");
			//be his own
			if (created == false){
			((EmptyKadProtocol) current.getProtocol(pid)).create();
			//KnockKnockClient.addNode(((EmptyKadProtocol) current.getProtocol(pid)).getKadId().toString());

			fail++;
			created = true;
			}
			
		}
	}

	public Node getBootStrapingNode() {
		String a;
		Random generator = new Random(CommonState.r.nextLong());
		int numberOfTry = 0;
		Node other;
		do {
			if (Network.size() <= 0)
				return null;
			other = Network.get(generator.nextInt(Network.size()));
			a = ((EmptyKadProtocol) other.getProtocol(pid)).getKadId()
					.toString();
			numberOfTry++;
			if (numberOfTry > 10) {
				return Network.get(0);
				//break;
			}
		} while (other == null
				|| other.isUp() == false
				);
		return other;
	}

}
