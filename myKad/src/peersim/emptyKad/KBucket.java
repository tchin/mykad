package peersim.emptyKad;

import java.math.BigInteger;
import java.util.TreeMap;

import peersim.core.CommonState;
import peersim.emptyKad.observer.StabilizeObserver;

/**
 * This class implements a kademlia k-bucket. Function for the management of the neighbours update are also implemented
 * 
 * @author Daniele Furlan, Maurizio Bonani
 * @version 1.0
 */
public class KBucket implements Cloneable {
	private BigInteger src;
	// k-bucket array
	protected TreeMap<BigInteger, Long> neighbours = null;

	// empty costructor
	public KBucket(BigInteger src) {
		neighbours = new TreeMap<BigInteger, Long>();
		this.src = src;
	}

	// add a neighbour to this k-bucket
	public void addNeighbour(BigInteger node) {

		long time = CommonState.getTime();
		if (neighbours.size() < KademliaCommonConfig.K) { // k-bucket isn't full
			neighbours.put(node, time); // add neighbour to the tail of the list
			KnockKnockClient.addNode(node.toString());
			//if (!(src.toString().equals(node.toString()))){
				KnockKnockClient.connectNode(src.toString(),node.toString());
				//System.out.println("adding " + src.toString() +"==" + node.toString());
			//}
		}
	}

	// remove a neighbour from this k-bucket
	public void removeNeighbour(BigInteger node) {
		neighbours.remove(node);
		
		KnockKnockClient.removeLine(node.toString());
		KnockKnockClient.deleteNode(node.toString());
	}

	public Object clone() {
		KBucket dolly = new KBucket(this.src);
		for (BigInteger node : neighbours.keySet()) {
			dolly.neighbours.put(new BigInteger(node.toByteArray()), 0l);
		}
		return dolly;
	}

	public String toString() {
		String res = "{\n";

		for (BigInteger node : neighbours.keySet()) {
			res += node + "\n";
		}
		return res + "}";
	}
}
