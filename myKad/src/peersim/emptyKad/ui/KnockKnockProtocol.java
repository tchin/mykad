package peersim.emptyKad.ui;

import java.math.BigInteger;
import java.net.*;
import java.io.*;

import javafx.concurrent.Task;


public class KnockKnockProtocol {
	public static final String magicNumber = "1234567890";
	private static final int WAITING = 0;
	private static final int SENTKNOCKKNOCK = 1;
	private static final int REGISTER = 2;
	private static final int REMOVE = 3;
	private static final int CONNECT = 4;
	private static final int CLEAR = 5;
	private static final int REMOVELINE = 6;
	private int state = WAITING;
	MainUIController mainUIController;

	public KnockKnockProtocol(MainUIController mainUIController) {
		this.mainUIController = mainUIController;
	}

	public String processInput(final String theInput) {
		String theOutput = null;

		if (state == WAITING) {
			theOutput = "Knock! Knock!";
			state = SENTKNOCKKNOCK;
		} else if (state == SENTKNOCKKNOCK) {
			if (theInput.equals("register")) {
				theOutput = "registerserver";
				state = REGISTER;
			} else if (theInput.equals("remove")){
				theOutput = "removeserver";
				state = REMOVE;
			} else if (theInput.equals("connect")) {
				theOutput = "connectserver";
				state = CONNECT;
			} else if (theInput.equals("clear")) {
				theOutput = "clearAll";
				state = CLEAR;
			} else if (theInput.equals("removeLine")) {
				theOutput = "removeLine";
				state = REMOVELINE;
			}  else if (theInput.equals("exit")) {
				theOutput = "Bye.";
			}
		} else if (state == REGISTER) {
			theOutput="regServer";
		} else if (state == REMOVE){
			theOutput="remServer";
		} else if (state == CONNECT){
			theOutput="conServer";
		}else if (state == CLEAR){
			theOutput="clearServer";
		}else if (state == REMOVELINE){
			theOutput="removeLineServer";
		} else {
			theOutput = "Bye.";
		}
		return theOutput;
	}
}