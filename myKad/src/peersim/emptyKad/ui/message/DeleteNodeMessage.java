package peersim.emptyKad.ui.message;

public class DeleteNodeMessage  extends ObjectUIMessage{
	public String Server;

	public DeleteNodeMessage(String server) {
		super();
		Server = server;
	}
}
