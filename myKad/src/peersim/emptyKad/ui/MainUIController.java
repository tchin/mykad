package peersim.emptyKad.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import peersim.emptyKad.ui.message.AddNodeMessage;
import peersim.emptyKad.ui.message.ClearAllMessage;
import peersim.emptyKad.ui.message.ConnectNodeMessage;
import peersim.emptyKad.ui.message.DeleteNodeMessage;
import peersim.emptyKad.ui.message.RemoveLineMessage;



import javafx.application.Application.Parameters;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.EllipseBuilder;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;

public class MainUIController implements Initializable {
	@FXML
	AnchorPane mainPane;
	Text nodeCounterText;
	long nodeCounter = 0;
	double radius;
	HashMap<BigInteger, Ellipse> listMap = new HashMap<BigInteger, Ellipse>();
	HashMap<BigInteger, Text> textMap = new HashMap<BigInteger, Text>();
	HashMap<BigInteger, Path> lineMap = new HashMap<BigInteger, Path>();
	HashMap<BigInteger, Path> revlineMap = new HashMap<BigInteger, Path>();
	public void drawCircle(BigInteger id) {
		
		double percentage = id.doubleValue()
				/ BigInteger.valueOf(2).pow(Main.idLength).doubleValue();
		double degree = percentage * 360;
		double x = Main.screenWidth / 2 + radius
				* Math.cos(degree * Math.PI / 180);
		double y = Main.screenHeight / 2 + radius
				* Math.sin(degree * Math.PI / 180);
		Text theText = TextBuilder.create().text(id.toString()).x(x + 3).y(y)
				.styleClass("Message1").build();
		Ellipse ellipse = EllipseBuilder.create().centerX(x).centerY(y)
				.radiusX(3).radiusY(3).strokeWidth(1).stroke(Color.BLACK)
				.fill(Color.BLACK).build();
		 listMap.put(id, ellipse);
		 textMap.put(id, theText);
		mainPane.getChildren().addAll(theText, ellipse);
		nodeCounterText.setText(nodeCounter+"");
		nodeCounter++;
		
	}
	public void removeCircle(BigInteger id) {
		mainPane.getChildren().remove(listMap.get(id));
		mainPane.getChildren().remove(textMap.get(id));
		
	}
	
	public void setNodeCount(){
		
		
	}
	public void drawLine(BigInteger id, BigInteger id2) {
		//check got existing line then delete it
		/*
		Path old = lineMap.get(id);
		if (old!=null){
	//		System.out.println("remove" + id.toString());
			mainPane.getChildren().remove(old);
		}
		*/
//		System.out.println("=" +id.toString());
//		System.out.println("-" +id2.toString());
		double percentage = id.doubleValue()
				/ BigInteger.valueOf(2).pow(Main.idLength).doubleValue();
		double degree = percentage * 360;
		double x = Main.screenWidth / 2 + radius
				* Math.cos(degree * Math.PI / 180);
		double y = Main.screenHeight / 2 + radius
				* Math.sin(degree * Math.PI / 180);
		
		
		double percentage2 = id2.doubleValue()
				/ BigInteger.valueOf(2).pow(Main.idLength).doubleValue();
		double degree2 = percentage2 * 360;
		double x2 = Main.screenWidth / 2 + radius
				* Math.cos(degree2 * Math.PI / 180);
		double y2 = Main.screenHeight / 2 + radius
				* Math.sin(degree2 * Math.PI / 180);	
		
		Path path = new Path();
	      MoveTo moveTo = new MoveTo();
	      moveTo.setX(x);
	      moveTo.setY(y);
	     
	      LineTo lineTo = new LineTo();
	      lineTo.setX(x2);
	      lineTo.setY(y2);

	      LineTo lineTo2 = new LineTo();
	      lineTo2.setX(x2 -10);
	      lineTo2.setY(y2 +5);
	    

	      path.getElements().add(moveTo);
	      path.getElements().add(lineTo);
	      path.getElements().add(lineTo2);
	   

	      
	      path.setStrokeWidth(1);
	      path.setStroke(Color.BLACK);	
		
	      lineMap.put(id, path );
	      revlineMap.put(id2,path);
	//      System.out.println("connect" + id.toString() + "=" + id2.toString());
		mainPane.getChildren().addAll(path);
		
	}	
	
	
	public void setRadius(double rad) {
		this.radius = rad;
	}
	public void startServer(){
		Task serverTask = new Task<Void>() {

			@Override
			public Void call() {
				ServerSocket serverSocket = null;
				//Socket clientSocket = null;
				try {
					serverSocket = new ServerSocket(5555);
					serverSocket.setReuseAddress(true);
					serverSocket.setReceiveBufferSize(Integer.MAX_VALUE);
					while (true) {
						try {
							final Socket clientSocket = serverSocket.accept();
							clientSocket.setReuseAddress(true);
							Task processingTask = new Task<Void>() {
	
								//final ObjectInputStream in;
					
								Object inputObject;
								@Override
								protected Void call() throws Exception {
									// TODO Auto-generated method stub
									
									
									try {
										
										ObjectInputStream in = new ObjectInputStream(
												clientSocket.getInputStream());
										
										

										// initiate conversation with client
										KnockKnockProtocol kkp = new KnockKnockProtocol(MainUIController.this);
										//outputLine = kkp.processInput(null);
										
										//out.println(outputLine);
										
										while ((inputObject = in.readObject()) != null) {
											
											final Object theObject = inputObject;
											
											
											Task uiTask = new Task<Void>() {
												String outputLine;
												String inputLine;
												String inputLine2;
												protected Void call() throws Exception {
													if (theObject instanceof AddNodeMessage){
														outputLine = "regServer";
														inputLine = ((AddNodeMessage)theObject).Server;
													} else if (theObject instanceof DeleteNodeMessage){
														outputLine = "remServer";
														inputLine = ((DeleteNodeMessage)theObject).Server;
													} else  if (theObject instanceof ConnectNodeMessage){
														outputLine = "conServer";
														inputLine = ((ConnectNodeMessage)theObject).from;
														inputLine2 = ((ConnectNodeMessage)theObject).to;
													} else if(theObject instanceof ClearAllMessage){
														outputLine = "clearServer";
													} else if (theObject instanceof RemoveLineMessage){
														outputLine = "removeLineServer";
														inputLine = ((RemoveLineMessage)theObject).from;
													}
													return null;
												}
												@Override
												protected void succeeded() {
													super.succeeded();
													if (outputLine.equals("regServer"))
														MainUIController.this.drawCircle(new BigInteger(inputLine));
													else if(outputLine.equals("remServer")){
														MainUIController.this.removeCircle(new BigInteger(inputLine));
//														System.out.println("removing" + inputLine);
													} else if (outputLine.equals("conServer")){
														MainUIController.this.drawLine(new BigInteger(inputLine), new BigInteger(inputLine2));
													}else if (outputLine.equals("clearServer")){
														MainUIController.this.clearAll();
													}else if (outputLine.equals("removeLineServer")){
														MainUIController.this.removeLine(new BigInteger(inputLine));
													}
												}
											};
											Thread c = new Thread(uiTask);
											c.start();
											
										}
										clientSocket.close();
									
										
									}
										catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									return null;
								}
								@Override
								protected void succeeded() {
									super.succeeded();
									
								}
							};
							Thread b = new Thread(processingTask);
							b.start();
							//Processing pro = new Processing(clientSocket, this);
							//pro.start();
				
						} catch (IOException e) {
							System.out.println("Accept failed: 5555");
							System.exit(-1);
						} 
					}					
				} catch (IOException e) {
					System.out.println("Could not listen on port: 5555");
					System.exit(-1);
				}

				return null;
			}

			@Override
			protected void succeeded() {
				super.succeeded();
			}
		};
		Thread a = new Thread(serverTask);
		a.setDaemon(true);
		a.start();		
	}
	
	public void clearAll(){
		mainPane.getChildren().clear();
		this.nodeCounter = 0;
		mainPane.getChildren().add(nodeCounterText);
	}
	public void removeLine(BigInteger id) {
		Path a = lineMap.get(id);
		
		//a.setBlendMode(BlendMode.EXCLUSION);
		//System.out.println("remove linesssssssssssssssssss");
		lineMap.remove(a);
		mainPane.getChildren().remove(a);
	}	
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		nodeCounterText = TextBuilder.create().text(0+"").x(Main.screenWidth * 0.80).y(Main.screenHeight * 0.10)
				.styleClass("Message2").build();
		mainPane.getChildren().addAll(nodeCounterText);
	}
}
