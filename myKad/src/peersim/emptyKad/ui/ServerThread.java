package peersim.emptyKad.ui;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread extends Thread {
	ServerSocket serverSocket;
	Socket clientSocket = null;
	MainUIController mainUIController;
	public ServerThread(ServerSocket serverSocket, MainUIController mainUIController) {
		super();
		this.serverSocket = serverSocket;
		this.mainUIController =mainUIController;
	}

	public void run() {
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				Processing pro = new Processing(clientSocket, mainUIController);
				pro.start();
	
			} catch (IOException e) {
				System.out.println("Accept failed: 5555");
				System.exit(-1);
			}
		}
	}
}
