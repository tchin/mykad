package peersim.emptyKad;

import java.math.BigInteger;
import java.util.ArrayList;

import peersim.core.CommonState;

public class KadIdGenerator {
	public static ArrayList<BigInteger> ids = new ArrayList();
	public static synchronized BigInteger generateID(int idLength) {
		BigInteger rand;
		boolean flag = false;
		do {
			flag = false;
			rand = new BigInteger(idLength, CommonState.r);
			for (BigInteger i : ids) {
				if (i.equals(rand)) {
					flag = true;
					break;
				}
			}
		} while (flag == true);
		ids.add(rand);
		return rand;

	}
}
