package peersim.emptyKad;

public class PeersimConfig {
	public static final String PAR_TRANSPORT = "transport";
	public static final String PAR_IDLENGTH = "idLength";
	public static final String PAR_SUCCSIZE = "succListSize";
	public static final String PAR_PROT = "protocol";
}
